# feaASTools

Tools to handle feature files (*.fea) as abstract syntax tree.

## Installation

```shell
pip install feaASTools
```

## Documentation

For details read the [Documentation](https://fontstuff.gitlab.io/feaASTools/).
