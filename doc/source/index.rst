
Welcome to the feaASTools documentation
===============================================================================

This package provides tools to handle feature files (\*.fea) 
as abstract syntax tree (AST).

.. toctree::
   :maxdepth: 2
   :caption: API documentation:

   feaASTools

Indices and tables
-------------------------------------------------------------------------------

* :ref:`genindex`
* :ref:`modindex`
